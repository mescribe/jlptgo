# jlptgo

http://jlptgo.com  
Extrait du super site http://jlptgo.com pour apprendre le Japonais.

# Commentaire
Hélas depuis début 2021 le site ne fonctionne plus correctement.  

Ici, vous trouverai un backup des :
 - documents du site 
 - des Anki files

## Anki files
Les Anki files (.anki) ont aussi été récupéré et converti pour la version 2 (.apkg).
Pour ceux qui n'utilise pas Anki, les bases ont été mis à plat dans des fixhiers texte (.txt).
